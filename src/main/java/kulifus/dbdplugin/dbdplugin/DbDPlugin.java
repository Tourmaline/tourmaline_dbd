package kulifus.dbdplugin.dbdplugin;

import kulifus.dbdplugin.dbdplugin.Listner.CustomListener;
import kulifus.dbdplugin.dbdplugin.Listner.PlayerListener;
import kulifus.dbdplugin.dbdplugin.command.DbDAdminCommand;
import kulifus.dbdplugin.dbdplugin.command.DbDCommand;
import kulifus.dbdplugin.dbdplugin.game.GameManager;
import kulifus.dbdplugin.dbdplugin.game.InstantCreatorManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class DbDPlugin extends JavaPlugin {

    //mapに最低存在しないとゲームを開始できない各オブジェクトの数
    public final int lampSize = 5;
    public final int fuckSize = 5;
    public final int gateSize = 2;

    //発電機の修理カウント
    public static final int generatorCount = 40;

    //フックでの耐久カウント
    public static final int hookCount = 50;

    private GameManager gameManager;
    private InstantCreatorManager instantCreatorManager;

    @Override
    public void onEnable() {

        this.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        this.getServer().getPluginManager().registerEvents(new CustomListener(this), this);

        this.getCommand("DbDAdmin").setExecutor(new DbDAdminCommand(this));
        this.getCommand("DbD").setExecutor(new DbDCommand(this));

        gameManager = new GameManager(this);
        instantCreatorManager = new InstantCreatorManager(this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic

    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public InstantCreatorManager getInstantCreatorManager() {
        return instantCreatorManager;
    }
}
