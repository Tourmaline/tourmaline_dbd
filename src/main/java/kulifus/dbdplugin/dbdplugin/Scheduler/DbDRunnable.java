package kulifus.dbdplugin.dbdplugin.Scheduler;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.display.Display;
import kulifus.dbdplugin.dbdplugin.events.FixGeneratorCountEvent;
import kulifus.dbdplugin.dbdplugin.events.GeneratorDamageEvent;
import kulifus.dbdplugin.dbdplugin.events.SkillCheckEvent;
import kulifus.dbdplugin.dbdplugin.game.Game;
import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.object.Hook;
import kulifus.dbdplugin.dbdplugin.player.DbDPlayer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Random;

public class DbDRunnable extends BukkitRunnable {

    private DbDPlugin plugin;
    private int count = 0;

    public DbDRunnable(DbDPlugin plugin){
        this.plugin = plugin;
    }

    @Override
    public void run(){
        //1sに1回の処理
        if(count % 20 == 0){

            //発電機関連の処理
            for(Game game : plugin.getGameManager().getGames()){
                for(Generator generator : game.getGenerators()){
                    //発電機がダメージ状態の時カウントを減らす処理
                    if(generator.getState().equals(Generator.GeneratorState.damaged)){
                        GeneratorDamageEvent generatorDamageEvent = new GeneratorDamageEvent(generator);
                        plugin.getServer().getPluginManager().callEvent(generatorDamageEvent);
                        generator.reduceCount(generatorDamageEvent.getDamage());
                    }
                    //発電機を修理中の人数分修理する処理
                    int fixCount = generator.getCount();
                    List<Survivor> survivors = generator.getFixSurvivors();
                    FixGeneratorCountEvent fixGeneratorCountEvent = new FixGeneratorCountEvent(fixCount, generator, survivors);
                    if(!fixGeneratorCountEvent.isCancelled()){
                        generator.addCount(fixGeneratorCountEvent.getCount());
                    }
                }
            }

            //フックに吊られている際のカウント処理
            for(DbDPlayer dbDPlayer : plugin.getGameManager().getNowGamePlayers()){
                Survivor survivor = null;
                if(dbDPlayer instanceof Survivor){
                    survivor = (Survivor)dbDPlayer;
                }
                if(survivor == null){
                    continue;
                }
                if(survivor.getHook() == null){
                    continue;
                }
                Hook hook = survivor.getHook();
                survivor.addCount(1);
                if(survivor.getCount() == DbDPlugin.hookCount){
                    survivor.getPlayer().sendMessage("あなたは死亡しました.");
                    survivor.setStop(true);
                    survivor.addState(Survivor.SurvivorState.dead);
                    //ここにサバイバーの死亡処理
                    hook.setBreak(true);
                }
            }
        }

        //5sに1回の処理
        if(count % 100 == 0){
            //発電機を修理する処理（プレイヤー側）
            for(DbDPlayer dbDPlayer : plugin.getGameManager().getNowGamePlayers()){
                Survivor survivor = null;
                if(dbDPlayer instanceof Survivor){
                    survivor = (Survivor)dbDPlayer;
                }
                if(survivor == null){
                    continue;
                }
                if(survivor.getFixGenerator() == null){
                    continue;
                }
                Generator generator = survivor.getFixGenerator();
                Random rand = new Random();
                int num = rand.nextInt(5);
                if(num == 0){
                    SkillCheckEvent skillCheckEvent = new SkillCheckEvent(survivor, generator);
                    plugin.getServer().getPluginManager().callEvent(skillCheckEvent);
                    generator.SkillCheck(survivor, skillCheckEvent.getBonusCount());
                }
            }

        }

        //毎tick処理
        for(DbDPlayer dbDPlayer : plugin.getGameManager().getNowGamePlayers()){
            Display.displayScoreboard(dbDPlayer, plugin);
        }

        count++;
        if(count > 100){
            count = 0;
        }
    }

}
