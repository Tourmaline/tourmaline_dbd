package kulifus.dbdplugin.dbdplugin.map;

import kulifus.dbdplugin.dbdplugin.CustomConfig;
import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DbDMap {

    private DbDPlugin plugin;

    private String name;
    private List<Location> lampLocations;
    private List<Location> hookLocations;
    private List<Location> gateLocations;

    public DbDMap(DbDPlugin plugin, String name, List<Location> lampLocations, List<Location> fuckLocations, List<Location> gateLocations){
        this.plugin = plugin;
        this.name = name;
        this.lampLocations = lampLocations;
        this.hookLocations = hookLocations;
        this.gateLocations = gateLocations;
    }

    public DbDMap(DbDPlugin plugin, String name){
        this.plugin = plugin;
        this.name = name;
        this.lampLocations = loadLampLocations();
        this.hookLocations = loadFuckLocations();
        this.gateLocations = loadGateLocations();
    }

    public boolean check(int lampSize, int fuckSize, int gateSize){
        if(lampSize <= lampLocations.size() &&
                fuckSize <= hookLocations.size() &&
                gateSize <= gateLocations.size()){
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public List<Location> getHookLocations() {
        return hookLocations;
    }

    public List<Location> getLampLocations() {
        return lampLocations;
    }

    public List<Location> getGateLocations() {
        return gateLocations;
    }

    private List<Location> loadLampLocations(){
        List<Location> result = new ArrayList<>();
        CustomConfig config = new CustomConfig(plugin, name + ".yml");
        FileConfiguration bukkitConfig = config.getConfig();
        int size = bukkitConfig.getInt("LampLocationSize");
        for(int i = 0; i <= size; i++){
            result.add(Location.deserialize((Map<String, Object>)bukkitConfig.getMapList("LampLocation." + size)));
        }
        return result;
    }

    private List<Location> loadFuckLocations(){
        List<Location> result = new ArrayList<>();
        CustomConfig config = new CustomConfig(plugin, name + ".yml");
        FileConfiguration bukkitConfig = config.getConfig();
        int size = bukkitConfig.getInt("HookLocationSize");
        for(int i = 0; i <= size; i++){
            result.add(Location.deserialize((Map<String, Object>)bukkitConfig.getMapList("HookLocation." + size)));
        }
        return result;
    }

    private List<Location> loadGateLocations(){
        List<Location> result = new ArrayList<>();
        CustomConfig config = new CustomConfig(plugin, name + ".yml");
        FileConfiguration bukkitConfig = config.getConfig();
        int size = bukkitConfig.getInt("GateLocationSize");
        for(int i = 0; i <= size; i++){
            result.add(Location.deserialize((Map<String, Object>)bukkitConfig.getMapList("GateLocation." + size)));
        }
        return result;
    }

    public static void setLampLocation(String mapName, DbDPlugin plugin, Location location){
        CustomConfig config = new CustomConfig(plugin, mapName +".yml");
        FileConfiguration bukkitConfig = config.getConfig();
        int size = bukkitConfig.getInt("LampLocationSize");
        bukkitConfig.set("LampLocation." + size, location.serialize());
        bukkitConfig.set("LampLocationSize", size + 1);
        config.saveConfig();
    }

    public static void setHookLocation(String mapName, DbDPlugin plugin, Location location){
        CustomConfig config = new CustomConfig(plugin, mapName +".yml");
        FileConfiguration bukkitConfig = config.getConfig();
        int size = bukkitConfig.getInt("HookLocationSize");
        bukkitConfig.set("HookLocation." + size, location.serialize());
        bukkitConfig.set("HookLocationSize", size + 1);
        config.saveConfig();
    }

    public static void setGateLocation(String mapName, DbDPlugin plugin, Location location){
        CustomConfig config = new CustomConfig(plugin, mapName +".yml");
        FileConfiguration bukkitConfig = config.getConfig();
        int size = bukkitConfig.getInt("GateLocationSize");
        bukkitConfig.set("GateLocation." + size, location.serialize());
        bukkitConfig.set("GateLocationSize", size + 1);
        config.saveConfig();
    }

}
