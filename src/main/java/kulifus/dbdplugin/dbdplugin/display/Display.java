package kulifus.dbdplugin.dbdplugin.display;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.game.Game;
import kulifus.dbdplugin.dbdplugin.player.DbDPlayer;
import kulifus.dbdplugin.dbdplugin.player.Killer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class Display {

    public static void displayScoreboard(DbDPlayer dbdplayer, DbDPlugin plugin){
        Player player = dbdplayer.getPlayer();

        Game game = plugin.getGameManager().getGame(dbdplayer);

        Scoreboard sb = player.getScoreboard();
        Objective obj = sb.registerNewObjective("Dead by Daylight in MLS", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);

        if(dbdplayer instanceof Survivor){
            Survivor survivor = (Survivor)dbdplayer;
            obj.getScore("------------------------------").setScore(99);
            obj.getScore(ChatColor.DARK_GREEN + "あなたのライフ: " + survivor.getLife()).setScore(98);
            obj.getScore(" ").setScore(97);
            obj.getScore(ChatColor.AQUA + "あなたの時間:" + (DbDPlugin.hookCount - survivor.getCount())).setScore(96);
            obj.getScore(" ").setScore(95);
            obj.getScore(ChatColor.GOLD + "残り逃走者数: " + game.getSurvivorSize()).setScore(94);
            obj.getScore((" ")).setScore(93);
            obj.getScore(ChatColor.BLUE + "残り発電機数: " + game.getGenerators().size()).setScore(92);
            obj.getScore("------------------------------").setScore(91);

        }else if(dbdplayer instanceof Killer){
            Killer killer = (Killer)dbdplayer;
            obj.getScore("------------------------------").setScore(99);
            obj.getScore(ChatColor.GOLD + "残り逃走者数: " + game.getSurvivorSize()).setScore(98);
            obj.getScore((" ")).setScore(97);
            obj.getScore(ChatColor.BLUE + "残り発電機数: " + game.getGenerators().size()).setScore(96);
            obj.getScore("------------------------------").setScore(95);
        }
    }

}
