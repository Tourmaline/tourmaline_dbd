package kulifus.dbdplugin.dbdplugin.command;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.game.InstantCreatorManager;
import kulifus.dbdplugin.dbdplugin.map.DbDMap;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.List;

public class DbDAdminCommand implements CommandExecutor {

    private DbDPlugin plugin;

    public DbDAdminCommand(DbDPlugin plugin){
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args){

        Player player;
        if(!(sender instanceof Player)){
            sender.sendMessage("this command is player only.");
            return false;
        }
        player = (Player)sender;

        if(args.length < 1){
            return false;
        }

        // /dbdadmin set lamp mapName 金斧でクリックした際に発電機の座標を保存する
        // /dbdadmin set hook mapName 金斧でクリックした際にフックの座標を保存する
        // /dbdadmin set gate mapName 金斧でクリックした際にゲートの座標を保存する
        if(args[0].equalsIgnoreCase("set")){

            if(args.length != 3){
                return false;
            }

            if(args[1].equalsIgnoreCase("lamp")){

                player.setMetadata("LampFileName", new FixedMetadataValue(plugin, args[2]));

            }else if(args[1].equalsIgnoreCase("hook")){

                player.setMetadata("HookFileName", new FixedMetadataValue(plugin, args[2]));

            }else if(args[1].equalsIgnoreCase("gate")){

                player.setMetadata("GateFileName", new FixedMetadataValue(plugin, args[2]));

            }

         // /dbdadmin game create killerName killerNameに指定したPlayerをkillerに設定したうえでゲーム（仮登録）を生成する。
         // /dbdadmin game add survivorName survivorNameに指定したPlayerをsurvivorとしてゲームに追加する。
         // /dbdadmin game set mapName mapNameで指定した名前のファイルからMapデータを読み込む。
         // /dbdadmin game start userName userNameで指定したPlayerが作成したゲーム（仮登録）のゲームを開始する。
        }else if(args[0].equalsIgnoreCase("game")){

            if(args.length != 3){
                return false;
            }

            if(args[1].equalsIgnoreCase("create")){

                Player killerPlayer = null;
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(p.getName().equals(args[2])){
                        killerPlayer = p;
                    }
                }
                if(killerPlayer == null){
                    player.sendMessage("指定されたPlayerはOnlineでないか存在しません。");
                    return true;
                }

                plugin.getInstantCreatorManager().createInstantCreator(player, killerPlayer);
                player.sendMessage(killerPlayer.getName() + "がKillerでゲーム（仮登録）を作成しました。");
                return true;

            }else if(args[1].equalsIgnoreCase("add")){

                InstantCreatorManager.InstantCreator instantCreator = null;
                for(InstantCreatorManager.InstantCreator i : plugin.getInstantCreatorManager().getInstantCreators()){
                    if(i.getUser().equals(player)){
                        instantCreator = i;
                    }
                }
                if(instantCreator == null){
                    player.sendMessage("まだゲーム（仮登録）を作成していません。");
                    return true;
                }

                Player survivorPlayer = null;
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(p.getName().equals(args[2])){
                        survivorPlayer = p;
                    }
                }
                if(survivorPlayer == null){
                    player.sendMessage("指定されたPlayerはOnlineでないか存在しません。");
                    return true;
                }

                instantCreator.addSurvivorPlayer(survivorPlayer);
                player.sendMessage(survivorPlayer.getName() + "をゲーム（仮登録）に追加しました。");
                return true;

            }else if(args[1].equalsIgnoreCase("set")){

                InstantCreatorManager.InstantCreator instantCreator = null;
                for(InstantCreatorManager.InstantCreator i : plugin.getInstantCreatorManager().getInstantCreators()){
                    if(i.getUser().equals(player)){
                        instantCreator = i;
                    }
                }
                if(instantCreator == null){
                    player.sendMessage("まだゲーム（仮登録）を作成していません。");
                    return true;
                }

                instantCreator.setMap(args[2]);
                player.sendMessage(args[2] + "をMapとして読み込みました。");
                return true;

            }else if(args[1].equalsIgnoreCase("start")){

                Player user = null;
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(p.getName().equals(args[2])){
                        user = p;
                    }
                }
                if(user == null){
                    player.sendMessage("指定されたPlayerはOnlineでないか存在しません。");
                    return true;
                }

                InstantCreatorManager.InstantCreator instantCreator = null;
                for(InstantCreatorManager.InstantCreator i : plugin.getInstantCreatorManager().getInstantCreators()){
                    if(i.getUser().equals(user)){
                        instantCreator = i;
                    }
                }
                if(instantCreator == null){
                    player.sendMessage(user.getName() + "はゲーム（仮登録）を作成していません。");
                    return true;
                }

                Player killerPlayer = instantCreator.getKillerPlayer();
                List<Player> survivorPlayers = instantCreator.getSurvivorPlayers();
                DbDMap map = instantCreator.getMap();

                plugin.getGameManager().gameCreate(killerPlayer, survivorPlayers, map);
                user.sendMessage("ゲームを開始しました。");
                player.sendMessage("ゲームを開始しました。");
                killerPlayer.sendMessage("ゲームを開始しました");
                for(Player p : survivorPlayers){
                    p.sendMessage("ゲームを開始しました");
                }
                return true;

            }


        }

        return true;
    }

}
