package kulifus.dbdplugin.dbdplugin.object;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.events.FixedGeneratorEvent;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {

    DbDPlugin plugin;

    private int count = 0;
    private GeneratorState state = GeneratorState.normal;
    private List<Survivor> fixSurvivors = new ArrayList<>();
    private Location  location;

    public Generator(DbDPlugin plugin, Location location){
        this.plugin = plugin;
        this.location = location;
    }

    public int getCount() {
        return count;
    }

    public void addCount(int count){
        this.count += count;
        if(this.count >= DbDPlugin.generatorCount){
            this.count = DbDPlugin.generatorCount;
            FixedGeneratorEvent fixedGeneratorEvent = new FixedGeneratorEvent(this, fixSurvivors);
            plugin.getServer().getPluginManager().callEvent(fixedGeneratorEvent);
            this.location.getBlock().setType(Material.REDSTONE_LAMP_ON);
            this.state = GeneratorState.fixed;
            for(Survivor survivor : fixSurvivors){
                survivor.setFixGenerator(null);
                survivor.setStop(false);
            }
        }
    }

    public void reduceCount(int count){
        this.count -= count;
        if(this.count < 0){
            this.count = 0;
        }
    }

    public Location getLocation() {
        return location;
    }

    public GeneratorState getState() {
        return state;
    }

    public List<Survivor> getFixSurvivors() {
        return new ArrayList<>(fixSurvivors);
    }

    public void addFixSurvivor(Survivor survivor){
        fixSurvivors.add(survivor);
    }

    public boolean removeFixSurvivor(Survivor survivor){
        if(fixSurvivors.contains(survivor)){
            fixSurvivors.remove(survivor);
            return true;
        }else {
            return false;
        }

    }

    public void setState(GeneratorState state) {
        this.state = state;
    }

    public enum GeneratorState{

        normal(0),
        fixed(1),
        damaged(2);

        private int num;

        private GeneratorState(int num){
            this.num = num;
        }
    }

    public void SkillCheck(Survivor survivor, int bonusCount){
        Player p = survivor.getPlayer();
        Random rand = new Random();
        int num = rand.nextInt(6) + 4;
        BukkitTask task = new BukkitRunnable(){
            int x = 1;
            @Override
            public void run(){
                p.sendTitle(SkillCheckTitle(x, num), "", 0, 4, 0);
                if(p.isSneaking() && num <= x && x <= num + 5){
                    addCount(bonusCount);
                }
                if(x == 15){
                    this.cancel();
                }
                x++;
            }
        }.runTaskTimer(plugin, 0, 4);
    }

    private String SkillCheckTitle(int count, int rand){

        if(rand <= 3 || 9 < rand){
            return null;
        }

        String result = "";
        for(int i = 1; i <= 15; i++){
            if(count == i){
                result += "◆";
            }else{
                if(rand <= i && i <= rand + 4){
                    result += "■";
                }else{
                    result += "□";
                }
            }
        }
        return result;
    }
}
