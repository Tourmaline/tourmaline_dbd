package kulifus.dbdplugin.dbdplugin.object;

import org.bukkit.Location;

public class Gate {

    private boolean open = false;
    private int count = 0;
    private Location location;

    public Gate(Location location){
        this.location = location;
    }

    public int getCount() {
        return count;
    }

    public void addCount(int count){
        this.count += count;
    }

    public boolean isOpen() {
        return open;
    }
}
