package kulifus.dbdplugin.dbdplugin.object;


import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.Location;

public class Hook {

    private Location location;
    private boolean isBreak = false;
    private Survivor survivor = null;

    public Hook(Location location){
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isBreak() {
        return isBreak;
    }

    public void setBreak(boolean aBreak) {
        isBreak = aBreak;
    }

    public void setSurvivor(Survivor survivor) {
        this.survivor = survivor;
    }

    public Survivor getSurvivor() {
        return survivor;
    }
}
