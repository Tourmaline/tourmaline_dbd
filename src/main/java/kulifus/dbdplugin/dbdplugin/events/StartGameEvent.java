package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.game.Game;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class StartGameEvent extends Event{

    private static final HandlerList handlerList = new HandlerList();

    private Game game;

    public StartGameEvent(Game game){
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }
}
