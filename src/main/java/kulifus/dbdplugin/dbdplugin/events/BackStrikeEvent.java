package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.player.Killer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;

public class BackStrikeEvent extends StrikeEvent {

    public BackStrikeEvent(Survivor survivor, Killer killer){
        super(killer, survivor);
        super.setDamage(0);
    }

}
