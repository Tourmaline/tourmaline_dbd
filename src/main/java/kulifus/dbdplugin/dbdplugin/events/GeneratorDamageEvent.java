package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.object.Generator;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GeneratorDamageEvent extends Event {

    private static final HandlerList handlerList = new HandlerList();

    private int damage = 1;
    private Generator generator;

    public GeneratorDamageEvent(Generator generator){
        this.generator = generator;
    }

    public Generator getGenerator() {
        return generator;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }

}
