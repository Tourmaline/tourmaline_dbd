package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SkillCheckEvent extends Event implements Cancellable{

    private static final HandlerList handlerList = new HandlerList();
    private boolean cancelled = false;

    private Survivor survivor;
    private Generator generator;
    private int bonusCount = 2;

    public SkillCheckEvent(Survivor survivor, Generator generator){
        this.survivor = survivor;
        this.generator = generator;
    }

    public Survivor getSurvivor() {
        return survivor;
    }

    public Generator getGenerator() {
        return generator;
    }

    public int getBonusCount() {
        return bonusCount;
    }

    public void setBonusCount(int bonusCount) {
        this.bonusCount = bonusCount;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }

}
