package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.List;

public class FixedGeneratorEvent extends Event {

    private static final HandlerList handlerList = new HandlerList();

    private Generator generator;
    private List<Survivor> survivors;

    public FixedGeneratorEvent(Generator generator, List<Survivor> survivors){
        this.generator = generator;
        this.survivors = survivors;
    }

    public Generator getGenerator() {
        return generator;
    }

    public List<Survivor> getSurvivors() {
        return new ArrayList<>(survivors);
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }

}
