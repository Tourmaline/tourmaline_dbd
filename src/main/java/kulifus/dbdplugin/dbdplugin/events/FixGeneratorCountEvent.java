package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.List;

public class FixGeneratorCountEvent extends Event implements Cancellable {

    private static final HandlerList handlerList = new HandlerList();
    private boolean cancelled = false;

    private int count;
    private Generator generator;
    private List<Survivor> survivors;

    public FixGeneratorCountEvent(int count, Generator generator, List<Survivor>survivors){
        this.count = count;
        this.generator = generator;
        this.survivors = survivors;
    }

    public Generator getGenerator() {
        return generator;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Survivor> getSurvivors() {
        return new ArrayList<>(survivors);
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }

}
