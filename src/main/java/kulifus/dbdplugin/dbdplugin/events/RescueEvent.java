package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.object.Hook;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RescueEvent extends Event implements Cancellable {

    private static final HandlerList handlerList = new HandlerList();
    private boolean cancelled = false;

    private Survivor survivor;
    private Survivor rescued;
    private Hook hook;

    public RescueEvent(Survivor survivor, Survivor rescued, Hook hook){
        this.survivor = survivor;
        this.rescued = rescued;
        this.hook = hook;
    }

    public Hook getHook() {
        return hook;
    }

    public Survivor getSurvivor() {
        return survivor;
    }

    public Survivor getRescued() {
        return rescued;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }

}
