package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.object.Hook;
import kulifus.dbdplugin.dbdplugin.player.Killer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class HookEvent extends Event implements Cancellable {

    private static final HandlerList handlerList = new HandlerList();
    private boolean cancelled = false;

    private Killer killer;
    private Survivor survivor;
    private Hook hook;

    public HookEvent(Killer killer, Survivor survivor, Hook hook){
        this.killer = killer;
        this.survivor = survivor;
        this.hook = hook;
    }

    public Survivor getSurvivor() {
        return survivor;
    }

    public void setSurvivor(Survivor survivor) {
        this.survivor = survivor;
    }

    public Killer getKiller() {
        return killer;
    }

    public Hook getHook() {
        return hook;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }

}
