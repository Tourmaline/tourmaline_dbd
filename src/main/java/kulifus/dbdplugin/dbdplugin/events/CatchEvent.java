package kulifus.dbdplugin.dbdplugin.events;

import kulifus.dbdplugin.dbdplugin.player.Killer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CatchEvent extends Event {

    private static final HandlerList handlerList = new HandlerList();

    private Survivor survivor;
    private Killer killer;

    public CatchEvent(Survivor survivor, Killer killer){
        this.survivor = survivor;
        this.killer = killer;
    }

    public Survivor getSurvivor() {
        return survivor;
    }

    public Killer getKiller() {
        return killer;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return  handlerList;
    }

}
