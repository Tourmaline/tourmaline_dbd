package kulifus.dbdplugin.dbdplugin.player;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import org.bukkit.entity.Player;

public class DbDPlayer {

    private Player player;

    private boolean isStop = false;

    public DbDPlayer(Player player){
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isStop() {
        return isStop;
    }

    public void setStop(boolean stop) {
        isStop = stop;
    }
}
