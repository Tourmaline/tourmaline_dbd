package kulifus.dbdplugin.dbdplugin.player;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.game.Game;
import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.object.Hook;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Survivor extends DbDPlayer {

    private int life = 2;
    private int count = 0;
    private List<SurvivorState> survivorStateList = new ArrayList<>();
    private Generator fixGenerator = null;
    private Hook hook = null;

    public Survivor(Player player){
        super(player);
    }

    public int getCount() {
        return count;
    }

    public void addCount(int count){
        this.count += count;
        if(this.count > DbDPlugin.hookCount){
            this.count = DbDPlugin.hookCount;
        }
    }

    public int getLife() {
        return life;
    }

    public void addLife(int life){
        this.life += life;
    }

    public void reduceLife(int life){
        this.life -= life;
        if(life < 0){
            this.life = 0;
        }
    }

    public List<SurvivorState> getSurvivorStateList() {
        return survivorStateList;
    }

    public boolean isFix(){
        if(fixGenerator == null){
            return false;
        }else{
            return true;
        }
    }

    public Generator getFixGenerator() {
        return fixGenerator;
    }

    public void setFixGenerator(Generator fixGenerator) {
        this.fixGenerator = fixGenerator;
    }

    public Hook getHook() {
        return hook;
    }

    public void setHook(Hook hook) {
        this.hook = hook;
    }

    public boolean addState(SurvivorState state){
        if(survivorStateList.contains(state)){
            return false;
        }else{
            survivorStateList.add(state);
            return true;
        }
    }

    public boolean removeState(SurvivorState state){
        if(survivorStateList.contains(state)){
            survivorStateList.remove(state);
            return true;
        }else{
            return false;
        }
    }

    public boolean containState(SurvivorState state){
        if(survivorStateList.contains(state)){
            return true;
        }else{
            return false;
        }
    }

    public enum SurvivorState{
        dead(0);

        private int id;

        private SurvivorState(int id){
            this.id = id;
        }
    }
}
