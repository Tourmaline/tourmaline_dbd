package kulifus.dbdplugin.dbdplugin.player;

import kulifus.dbdplugin.dbdplugin.game.Game;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Killer extends DbDPlayer {

    private List<Survivor> catchSurvivors = new ArrayList<>();

    public Killer(Player player){
        super(player);
    }

    public List<Survivor> getCatchSurvivors() {
        return new ArrayList<>(catchSurvivors);
    }

    public void addSurvivor(Survivor survivor){
        catchSurvivors.add(survivor);
    }

    public boolean removeSurvivor(Survivor survivor){
        if(catchSurvivors.contains(survivor)){
            catchSurvivors.remove(survivor);
            return true;
        }else {
            return false;
        }
    }
}
