package kulifus.dbdplugin.dbdplugin.game;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.events.StartGameEvent;
import kulifus.dbdplugin.dbdplugin.map.DbDMap;
import kulifus.dbdplugin.dbdplugin.object.Hook;
import kulifus.dbdplugin.dbdplugin.object.Gate;
import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.player.DbDPlayer;
import kulifus.dbdplugin.dbdplugin.player.Killer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameManager {

    private DbDPlugin plugin;

    private List<Game> games = new ArrayList<>();
    private List<DbDPlayer> nowGamePlayers = new ArrayList<>();

    public GameManager(DbDPlugin plugin){
        this.plugin = plugin;
    }

    public List<DbDPlayer> getNowGamePlayers() {
        return nowGamePlayers;
    }

    public List<Game> getGames() {
        return games;
    }

    public DbDPlayer getDbDPlayer(Player player){
        for(DbDPlayer dbDPlayer :  nowGamePlayers){
            if(dbDPlayer.getPlayer().equals(player)){
                return dbDPlayer;
            }
        }
        return null;
    }

    public Game getGame(DbDPlayer dbDPlayer){
        for(Game game : games){
            if(game.getKiller(dbDPlayer.getPlayer()) != null){
                return game;
            }
            if(game.getSurvivor(dbDPlayer.getPlayer()) != null){
                return game;
            }
        }
        return null;
    }

    public void gameCreate(Player killerPlayer, List<Player> survivorPlayers, DbDMap map){
        if(!map.check(plugin.lampSize, plugin.fuckSize, plugin.gateSize)){
            plugin.getLogger().info("Error:10001");
            return;
        }

        //作成の際に指定されたプレイヤーが他のゲームに参加中ならエラーを返す。
        for(DbDPlayer dbDPlayer :nowGamePlayers){
            if(dbDPlayer.getPlayer().equals(killerPlayer)){
                plugin.getLogger().info("Error:20001");
                return;
            }
            for(Player player : survivorPlayers){
                if(dbDPlayer.getPlayer().equals(player)){
                    plugin.getLogger().info("Error:20002");
                    return;
                }
            }
        }

        //キラーが生存者として登録されてたらエラーを返す。
        for(Player player : survivorPlayers){
            if(player.equals(killerPlayer)){
                plugin.getLogger().info("Error:2003");
                return;
            }
        }

        List<Generator> generators = new ArrayList<>();
        for(Location loc : map.getLampLocations()){
            generators.add(new Generator(plugin, loc));
            loc.getBlock().setType(Material.REDSTONE_LAMP_OFF);
        }

        List<Hook> hooks = new ArrayList<>();
        for(Location loc : map.getHookLocations()){
            hooks.add(new Hook(loc));
            loc.getBlock().setType(Material.STONE_PLATE);
        }

        List<Gate> gates = new ArrayList<>();
        for(Location loc : map.getGateLocations()){
            gates.add(new Gate(loc));
        }

        Killer killer = new Killer(killerPlayer);
        nowGamePlayers.add(killer);
        List<Survivor> survivors = new ArrayList<>();
        for(Player p : survivorPlayers){
            Survivor survivor =  new Survivor(p);
            survivors.add(survivor);
            nowGamePlayers.add(survivor);
        }

        Game game = new Game(plugin, generators, hooks, survivors, killer);
        StartGameEvent event = new StartGameEvent(game);
        plugin.getServer().getPluginManager().callEvent(event);
        games.add(game);
        Random rand = new Random();
        int num = rand.nextInt(game.getHooks().size());
        Location loc = game.getHooks().get(num).getLocation();
        killerPlayer.teleport(loc);
        Random rand1 =  new Random();
        int num1 = rand1.nextInt(game.getGenerators().size());
        Location loc1 = game.getGenerators().get(num1).getLocation();
        for(Player p : survivorPlayers){
            p.teleport(loc1);
        }

    }



}
