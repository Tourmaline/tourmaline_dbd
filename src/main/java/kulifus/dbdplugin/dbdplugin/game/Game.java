package kulifus.dbdplugin.dbdplugin.game;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.object.Hook;
import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.player.Killer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.entity.Player;

import java.util.List;

public class Game {

    private DbDPlugin plugin;

    private List<Generator> generators;
    private List<Hook> hooks;

    private List<Survivor> survivors;
    private Killer killer;

    public Game(DbDPlugin plugin, List<Generator> generators, List<Hook> hooks, List<Survivor> survivors, Killer killer){
        this.plugin = plugin;
        this.generators = generators;
        this.hooks = hooks;
        this.survivors = survivors;
        this.killer = killer;
    }

    public Survivor getSurvivor(Player player){
        for(Survivor survivor : survivors){
            if(survivor.getPlayer().equals(player)){
                return survivor;
            }
        }
        return null;
    }

    public int getSurvivorSize(){
        return survivors.size();
    }

    public Killer getKiller(Player player){
        if(killer.getPlayer().equals(player)){
            return killer;
        }
        return null;
    }

    public Killer getKiller() {
        return killer;
    }

    public List<Hook> getHooks() {
        return hooks;
    }

    public List<Generator> getGenerators() {
        return generators;
    }
}
