package kulifus.dbdplugin.dbdplugin.game;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.map.DbDMap;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class InstantCreatorManager {

    private DbDPlugin plugin;

    private List<InstantCreator> instantCreators = new ArrayList<>();

    public InstantCreatorManager(DbDPlugin plugin){
        this.plugin = plugin;
    }

    public void createInstantCreator(Player user, Player killerPlayer){
        instantCreators.add(new InstantCreator(user, killerPlayer));
    }

    public void removeInstantCreator(InstantCreator instantCreator){
        if(instantCreators.contains(instantCreator)){
            instantCreators.remove(instantCreator);
        }
    }

    public List<InstantCreator> getInstantCreators() {
        return new ArrayList<>(instantCreators);
    }

    public class InstantCreator{

        private Player user;

        private DbDMap map = null;
        private Player killerPlayer;
        private List<Player> survivorPlayers = new ArrayList<>();

        public InstantCreator(Player user, Player killerPlayer){
            this.user = user;
            this.killerPlayer = killerPlayer;
        }

        public Player getUser() {
            return user;
        }

        public Player getKillerPlayer() {
            return killerPlayer;
        }

        public boolean setMap(String mapName){
            this.map = new DbDMap(plugin, mapName);
            if(this.map.check(plugin.lampSize, plugin.fuckSize, plugin.gateSize)){
                return true;
            }else{
                return false;
            }
        }

        public DbDMap getMap() {
            return map;
        }

        public List<Player> getSurvivorPlayers() {
            return new ArrayList<>(survivorPlayers);
        }

        public boolean addSurvivorPlayer(Player survivorPlayer){
            if(!survivorPlayers.contains(survivorPlayer)){
                survivorPlayers.add(survivorPlayer);
                return true;
            }else{
                return false;
            }
        }
    }

}
