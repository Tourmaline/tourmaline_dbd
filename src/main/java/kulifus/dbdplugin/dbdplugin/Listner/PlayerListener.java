package kulifus.dbdplugin.dbdplugin.Listner;

import kulifus.dbdplugin.dbdplugin.DbDPlugin;
import kulifus.dbdplugin.dbdplugin.events.*;
import kulifus.dbdplugin.dbdplugin.game.Game;
import kulifus.dbdplugin.dbdplugin.map.DbDMap;
import kulifus.dbdplugin.dbdplugin.object.Generator;
import kulifus.dbdplugin.dbdplugin.object.Hook;
import kulifus.dbdplugin.dbdplugin.player.DbDPlayer;
import kulifus.dbdplugin.dbdplugin.player.Killer;
import kulifus.dbdplugin.dbdplugin.player.Survivor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class PlayerListener implements Listener{

    private DbDPlugin plugin;

    public PlayerListener(DbDPlugin plugin){
        this.plugin = plugin;
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e){
        DbDPlayer dbDPlayer = plugin.getGameManager().getDbDPlayer(e.getPlayer());

        if(dbDPlayer != null && dbDPlayer.isStop()){
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onBlockClick(PlayerInteractEvent e){
        Block block = e.getClickedBlock();
        Player player = e.getPlayer();

        //右クリック時の処理
        if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            //レッドストーンランプ（発電機）をクリックしたときの処理
            if(block.getType().equals(Material.REDSTONE_LAMP_OFF)){
                Survivor survivor = null;
                Killer killer = null;
                for(DbDPlayer dbDPlayer : plugin.getGameManager().getNowGamePlayers()){
                    if(dbDPlayer.getPlayer().equals(player)){
                        if(dbDPlayer instanceof Survivor){
                            survivor = (Survivor)dbDPlayer;
                        }else if(dbDPlayer instanceof Killer){
                            killer = (Killer)dbDPlayer;
                        }
                    }
                }

                Game game =  plugin.getGameManager().getGame((DbDPlayer)survivor);

                Generator generator = null;
                for(Generator generator1 : game.getGenerators()){
                    if(generator1.getLocation().equals(block.getLocation())){
                        generator = generator1;
                    }
                }
                if(generator == null && generator.getState().equals(Generator.GeneratorState.fixed)){
                    return;
                }

                //発電機をクリックしたのが生存者だったら
                if(survivor != null){
                    if(survivor.isFix()){
                        survivor.setFixGenerator(null);
                        survivor.setStop(false);
                        generator.removeFixSurvivor(survivor);
                    }else {
                        FixGeneratorEvent event = new FixGeneratorEvent(survivor, generator);
                        plugin.getServer().getPluginManager().callEvent(event);
                        if (!event.isCancelled()) {
                            generator.setState(Generator.GeneratorState.normal);
                            survivor.setFixGenerator(generator);
                            survivor.setStop(true);
                            generator.addFixSurvivor(survivor);
                        }
                    }
                }

                //発電機をクリックしたのがキラーだったら
                if(killer != null){
                    GeneratorDamagedEvent generatorDamagedEvent = new GeneratorDamagedEvent(killer, generator);
                    plugin.getServer().getPluginManager().callEvent(generatorDamagedEvent);
                    if(!generatorDamagedEvent.isCancelled()){
                        generator.setState(Generator.GeneratorState.damaged);
                    }
                }


                //石の感圧版（フック）を踏んだ時の処理
            }else if (block.getType().equals(Material.STONE_PLATE)) {
                Killer killer = null;
                Survivor survivor = null;
                for (DbDPlayer dbDPlayer : plugin.getGameManager().getNowGamePlayers()) {
                    if (dbDPlayer.getPlayer().equals(player)) {
                        if (dbDPlayer instanceof Killer) {
                            killer = (Killer) dbDPlayer;
                        } else if (dbDPlayer instanceof Survivor) {
                            survivor = (Survivor) dbDPlayer;
                        }
                    }
                }

                if (killer == null && survivor == null) {
                    return;
                }

                Hook hook = null;
                for (Hook hook1 : plugin.getGameManager().getGame((DbDPlayer) killer).getHooks()) {
                    if (block.getLocation().equals(hook1.getLocation())) {
                        hook = hook1;
                    }
                }
                if (hook == null) {
                    return;
                }

                //フックを踏んだのがキラーだったら現在持っている生存者をフックに吊るす
                if (killer != null) {

                    if (hook.getSurvivor() != null && hook.isBreak()) {
                        return;
                    }

                    Survivor survivor1 = null;
                    for (Survivor survivor2 : killer.getCatchSurvivors()) {
                        survivor1 = survivor2;
                    }
                    if (survivor1 == null) {
                        return;
                    }

                    HookEvent event = new HookEvent(killer, survivor, hook);
                    plugin.getServer().getPluginManager().callEvent(event);
                    if (!event.isCancelled()) {
                        killer.removeSurvivor(event.getSurvivor());
                        event.getSurvivor().getPlayer().teleport(hook.getLocation().add(0, 1, 0));
                        event.getSurvivor().setHook(hook);
                        event.getSurvivor().setStop(true);
                        hook.setSurvivor(survivor);
                    }
                    //フックを踏んだのが生存者なら現在吊られている生存者をフックから助ける
                } else if (survivor != null) {

                    if (hook.getSurvivor() == null && hook.isBreak()) {
                        return;
                    }

                    Survivor rescued = hook.getSurvivor();

                    RescueEvent event = new RescueEvent(survivor, rescued, hook);
                    plugin.getServer().getPluginManager().callEvent(event);

                    if (!event.isCancelled()) {
                        rescued.setHook(null);
                        rescued.setStop(false);
                        hook.setSurvivor(null);
                    }

                }

            }

        //左クリック時の処理
        }else if(e.getAction().equals(Action.LEFT_CLICK_BLOCK)){
            //金の斧でブロックを左クリック＝マップのオブジェクトをファイルに保存
            if(player.getInventory().getItemInMainHand().getType().equals(Material.GOLD_AXE)){
                //クリックする前にコマンドでメタデータを設定している場合
                if(player.hasMetadata("LampFileName")){

                    String fileName = player.getMetadata("LampFileName").get(0).asString();
                    DbDMap.setLampLocation(fileName, plugin, block.getLocation());
                    player.removeMetadata("LampFileName", plugin);

                }else if(player.hasMetadata("HookFileName")){

                    String fileName = player.getMetadata("HookFileName").get(0).asString();
                    DbDMap.setHookLocation(fileName, plugin, block.getLocation());
                    player.removeMetadata("HookFileName", plugin);

                }else if(player.hasMetadata("GateFileName")){

                    String fileName = player.getMetadata("GateFileName").get(0).asString();
                    DbDMap.setGateLocation(fileName, plugin, block.getLocation());
                    player.removeMetadata("GateFileName", plugin);

                }
                e.setCancelled(true);
            }

        }



    }

    @EventHandler
    public void onDamaged(EntityDamageByEntityEvent e){
        Killer killer = null;
        if(e.getDamager() instanceof Player){
            Player player = (Player)e.getDamager();
            for(DbDPlayer dbDPlayer : plugin.getGameManager().getNowGamePlayers()){
                if(dbDPlayer.getPlayer().equals(player) && dbDPlayer instanceof Killer){
                    killer = (Killer)dbDPlayer;
                }
            }
        }
        if(killer == null){
            return;
        }
        Survivor survivor = null;
        if(e.getEntity() instanceof Player){
            Player player = (Player)e.getEntity();
            for(DbDPlayer dbDPlayer : plugin.getGameManager().getNowGamePlayers()){
                if(dbDPlayer.getPlayer().equals(player) && dbDPlayer instanceof Survivor){
                    survivor = (Survivor)dbDPlayer;
                }
            }
        }
        if(survivor == null){
            return;
        }

        if(survivor.isFix()){
            BackStrikeEvent event = new BackStrikeEvent(survivor, killer);
            plugin.getServer().getPluginManager().callEvent(event);
            CatchEvent event1 = new CatchEvent(survivor, killer);
            plugin.getServer().getPluginManager().callEvent(event1);
            survivor.setStop(true);
            survivor.reduceLife(100);
            killer.addSurvivor(survivor);
            return;
        }

        StrikeEvent event = new StrikeEvent(killer, survivor);
        plugin.getServer().getPluginManager().callEvent(event);
        if(!event.isCancelled()){
            survivor.reduceLife(event.getDamage());
            if(survivor.getLife() > 0){
                Player player = survivor.getPlayer();
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 40, 1));
            }else{
                CatchEvent event1 = new CatchEvent(survivor, killer);
                plugin.getServer().getPluginManager().callEvent(event1);
                killer.addSurvivor(survivor);
                survivor.setStop(true);
            }
        }

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player player = e.getPlayer();
        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
    }

}
